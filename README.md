# Boto
Boto is an attempt to provide features similar to what [Pathom](https://pathom3.wsscode.com/) provides to Clojure/Clojurescript.

The core concept behind Boto is that you won't have to think of entities anymore, you only need to care about the data that you already have and the data that you need.
Boto will take care of trying to retrieve all the data that you need, based on resolvers that you registered.
Imagine something like graphql but inside your code, that can leverage any source of information that you can reach.

## Work in progress
Boto is still in development and there is a lot of work to be done for it to be as useful as it can be.
There are some considerations on current behaviour:
- All resolvers are called in a single process, even for nested attributes.
- Currently queries are optimistic, what means that if Boto can't resolve everything you queried it gonna return everything it was able to retrieve.
- For this first experiment, I'm not handling errros for resolvers. So it's just expected for the resolvers to return data correctly.

If those behaviors will be kept or changed, it's open to discussion.

## [Current Roadmap](https://codeberg.org/cevado/boto/projects/1554)
- [i] Document the entire codebase.
- [i] Full test coverage of complex scenarios and unit tests.
- [d] Handle resolvers failures, with configurable retries.
- [d] Failure state when it's not possible to retrieve all data queried.
- [d] Additional parameter to nested queries.
- [d] Filters for nested attributes that return N entries.
- [c] Paralelize resolvers.
- [c] Cache with TTL for results.
[d]: discussion
[c]: confirmed
[i]: in progress
[x]: done


## [Examples](examples/)
There are two examples available, both in Elixir and Erlang, showing some scenarios of how to use the library.
The simple one is the weather resolver, that can get the current temperature forecast for a given ip, if no ip is provided it gonna use your own ip as resource.
The elaborated example is the Hacker News api resolver, that can query the public Hacker News api, including nested resources.
There is also a livemd file that you can open with [Livebook](https://livebook.dev/) to explore the library usage.

For reference when reading the docs follows a dummy implementation of `boto_resolver` behaviour.
### Erlang
``` erlang
-module(resolver).
-behaviour(boto_resolver).
-export([resolver_init/0, resolve/2]).

resolver_init() ->
    [{resolver1, [{output, [attribute1, attribute2, attribute3]}]},
     {resolver2, [{input, [attribute1]}, {output, [attribute4, attribute5, attribute6]}]}].

resolve(resolver1, Input) ->
    #{attribute1 => 1, attribute2 => 2, attribute3 => 3};
resolve(resolver2, #{attribute1 := Att1}) ->
    case Att1 of
        1 ->
            #{attribute4 => 4, attribute5 => 5, attribute6 => 6},
        2 ->
            #{attribute4 => 8, attribute5 => 10, attribute6 => 12}
    end.
```

### Elixir
``` elixir
defmodule Resolver do
    @behaviour :boto_resolver
    
    def resolver_init() do
    [
     resolver1: [output: [:attribute1, :attribute2, :attribute3]],
     resolver2: [input: [:attribute1], output: [:attribute4, :attribute5, :attribute6]]
    ]
    end
    
    def resolve(:resolver1, _input) do
        %{attribute1: 1, attribute2: 2, attribute3: 3}
    end
    
    def resolve(:resolver2, %{attribute1: att}) do
        case att do
            1 -> %{attribute4: 4, attribute5: 5, attribute6: 6}
            2 -> %{attribute4: 8, attribute5: 10, attribute6: 12}
        end
    end
end
```

## Contributing
The source of truth for this code is [codeberg](https://codeberg.org/cevado/boto), provide feedback and join discussion in the issues.
A mirror is kept in [sourcehut](https://git.sr.ht/~cevado/boto).
You're welcomed to interact on both places.
Check the [code of conduct](CODE_OF_CONDUCT.md).
