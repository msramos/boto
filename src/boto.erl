%% @doc Public api for Boto.
%% {@link boto_server} must be started, properly configured and with set of implementations of {@link boto_resolver} for boto to work.
%% @end
-module(boto).

-export([resolves/1, resolves/2, query/2]).

%% @doc Checks if the provided attribute is available in any resolver registered to boto.
%% @end
resolves(Output) ->
    {ok, Graph} = boto_worker:graph(),
    Vertices = digraph:vertices(Graph),
    lists:member(Output, Vertices).

%% @doc Checks if the input attribute can reach the output attribute.
%% If output attribute is reacheable through a resolver that doesn't require input attributes, it will return true as well.
%% @end
resolves(Input, Output) ->
    {ok, Graph} = boto_worker:graph(),
    Path = digraph:get_short_path(Graph, Input, Output),
    lists:member(Input, Path) and lists:member(noarg, Path).

%% @doc Retrieves attributes present in the query using the Input attributes provided.
%% @end
query(Input, Output) when is_map(Input), is_list(Output) ->
    boto_query:query(Input, Output).
