%% @private
-module(boto_graph).

-export([start/0, add_vertex/2, add_edge/4, path/3]).

start() ->
    digraph:new([cyclic]).

add_vertex(Graph, Vertex) ->
    digraph:add_vertex(Graph, Vertex).

add_edge(Graph, Vertex1, Vertex2, Label) ->
    digraph:add_edge(Graph, Vertex1, Vertex2, Label).

path(Graph, Vertex1, Vertex2) ->
    Path = digraph:get_short_path(Graph, Vertex1, Vertex2),
    build_path(Path, [], Graph).

get_edge(Graph, Vertex1, Vertex2) ->
    Edges = digraph:edges(Graph, Vertex1),
    Edges1 = [digraph:edge(Graph, E) || E <- Edges],
    Edges2 = [E || E <- Edges1, element(3, E) == Vertex2],
    Edges3 = [element(4, E) || E <- Edges2],
    hd(Edges3).

build_path(false, _Path, _Graph) ->
    [];
build_path([Vertex1, Vertex2], Path, Graph) ->
    Edge = get_edge(Graph, Vertex1, Vertex2),
    lists:reverse([Edge | Path]);
build_path([Vertex1, Vertex2 | Rest], Path, Graph) ->
    E = get_edge(Graph, Vertex1, Vertex2),
    build_path([Vertex2 | Rest], [E | Path], Graph).
